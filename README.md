
  

  

# # trip-check

  

  

-  [Installation](#installation)

  

-  [Team](#team)

---

  

## Installation

  

- All the `code` required to get started is PHP 7+ and MySQL

  
  

### Clone

  

- Clone this repository to your local machine using `git clone https://a101334@bitbucket.org/a101334/trip-chek.git`

  

*PS:* The username and password in database is developer , If the username and password not match with your machine, you need change this values in trip-chek/model/DAO/genericDAO.php.

  

### Setup manually

  

> start database

  

- run this commands in the path trip-chek/model/script-database/

  

```shell
$ mysql -u developer -p
$ source script-trip.sql
```

  

### To get started

  

> installation, clone and setup is did.
-  **Step 1**

  

- watch the video in trip-chek/media/poc-trip-check-2019-12-29_23.36.29.mkv

  

-  **Step 2**

  

- run in the root folder

  

```shell
$ php -S localhost:8080
```

  

## Team

  

>  [a101334 (Alex da Costa)](https://www.linkedin.com/in/alex-costa-92ba39163/)

  

---

  

## License

  
  

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

  

-  **[MIT license](http://opensource.org/licenses/mit-license.php)**
