<?php
    session_start();
    require_once  __DIR__ . '/../model/DAO/tripDAO.php';

    if (isset($_POST['idTrip']) && !is_null($_POST['idTrip'])) {
        $idTrip = $_POST['idTrip'];
        $tripDAO = new TripDAO();
        $resultItems = $tripDAO->getAllItemsFromTrip($idTrip);
        $resulTripCheck = $tripDAO->getTripCheck($idTrip);

        if(count($resulTripCheck) <= 0) {
            $resulTripCheck = $tripDAO->createTripCheckEmpty($idTrip, $_SESSION['idEmployee']);
        }

        echo json_encode(array($resulTripCheck, $resultItems));
    }

    if(isset($_POST['idItemTrip']) && !is_null($_POST['idItemTrip']) && isset($_POST['checkItemValue']) && !is_null($_POST['checkItemValue'])) {
        $idItemTrip = $_POST['idItemTrip'];
        $checkItemValue = $_POST['checkItemValue'];

        $tripDAO = new TripDAO();
        $tripDAO->updateCheckItemValue($idItemTrip, $checkItemValue);
    }

    if (isset($_POST['updateTripCheck'])) {
        $targetFile = '';

        $idTrip = $_POST['id_trip'];
        $tripDAO = new TripDAO();
        $resulTripCheckArray = $tripDAO->getTripCheck($idTrip);
        $resulTripCheck = $resulTripCheckArray[0];
        $urlNFE = $_POST['nfe'];
        $urlCTE = $_POST['cte'];
        $urlManifesto = $resulTripCheck['url_manifesto'];
        $typeTrip = $resulTripCheck['type_trip'];
        
        $isValidURL = false;

        if($typeTrip == 'importation') {
            if(!is_null($urlCTE) && !empty($urlCTE)) {
                $isValidURL = true;
            }
        } else {
            if(!is_null($urlNFE) && !empty($urlNFE)) {
                $isValidURL = true;
            }
        }

        if(!is_null($urlManifesto) && !empty($urlManifesto)) {
            $targetFile = $urlManifesto;
        } else {
            $targetFile = 'manifestos/'.basename(md5($_FILES['manifesto']['name']).'.pdf');
        }

        if((move_uploaded_file($_FILES["manifesto"]["tmp_name"], $targetFile) || !is_null($urlManifesto)) && count($tripDAO->isItemsTripCheck($idTrip) ) == 0 && $isValidURL) {
            $tripDAO->updateTripCheck($idTrip, 'Approved', $urlNFE, $urlCTE, $targetFile);
            echo 'Approved';
        } else {
            $tripDAO->updateTripCheck($idTrip, 'Pending', $urlNFE, $urlCTE, $targetFile);
            echo 'Pending';
        }

        echo '   <a href="/">back home</a><br>';
    }
?>