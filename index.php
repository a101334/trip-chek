<?php
    session_start();
    $_SESSION['idEmployee'] = 3;

?> 

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <section class="row justify-content-center">
            <article class="row col-10 justify-content-center">
                <h1>Trip</h1>
                <div class="row justify-content-center col-12">
                    <input id="tripId" type="text" class="col-8"> 
                    <button id="searchTrip">Search Trip</button>
                </div>
                <div class="row justify-content-center col-12">
                    <form method="POST" id="tripCheckForm" enctype="multipart/form-data" action="controller/tripController.php" class="col-12">
                        
                    </form>
                </div>
            </article>
           <hr class="col-12">
           <article class="row col-10 justify-content-center">
               <h1>Items Trip</h1>
               <table id="tableItemsTrip" class="table table-striped col-12"></table>
           </article>
        </section>
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>