const updateItemCheck = (obj) => {
    let checkItemValue = 0;
    const idItemTrip = obj.getAttribute('data-id-item-trip');
    
    if(obj.checked) {
        checkItemValue = 1;
    } 

    $.post('controller/tripController.php', { idItemTrip, checkItemValue }, (data, status) => {});
};

$(document).ready(() => {

    $('#searchTrip').click(() => {
        
        const idTrip = $('#tripId')[0].value;
        $.post('controller/tripController.php', { idTrip }, (data, status) => {
            const response = JSON.parse(data);
            const tripCheck = response[0][0];
            const itemsTrip = response[1];
            let styleStatusCheckTrip = `style="color:red;"`;

            if (tripCheck.status != 'Pending') {
                styleStatusCheckTrip = `style="color:blue;"`;
            } 

            let formTripCheck = $('#tripCheckForm')[0];
            formTripCheck.innerHTML = '';
            formTripCheck.innerHTML += `
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">ID TRIP: </label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="id_trip" name="id_trip" value="${tripCheck.id}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">STATUS: </label>
                                            <div class="col-sm-8">
                                            <input type="text" ${styleStatusCheckTrip}  readonly class="form-control-plaintext" id="status" value="${tripCheck.status}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">NOME FISCAL: </label>
                                            <div class="col-sm-8">
                                            <input type="text" readonly class="form-control-plaintext" id="fiscal_name" value="${tripCheck.name}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">Manifesto</label>
                                            <input type="file" class="form-control-file" id="manifesto" name="manifesto">
                                        </div>`;

            if(tripCheck.url_manifesto && tripCheck.url_manifesto != null) {
                formTripCheck.innerHTML += `<div class="form-group row">
                                                <label for="staticEmail" class="col-sm-4 col-form-label"> <a href="controller/${tripCheck.url_manifesto}" target="_blank"> Manifesto </a> </label>
                                            </div>`
            }
            
            if (tripCheck.type_trip == 'importation') {
                if(tripCheck.url_CTE && tripCheck.url_CTE != null) {
                    formTripCheck.innerHTML += `<div class="form-group row">
                                                    <label class="col-sm-4 col-form-label"> <a href="${tripCheck.url_CTE}" target="_blank"> CTe </a> </label>
                                                </div>`
                } 
                formTripCheck.innerHTML += `<div class="form-group row">
                                                <label class="col-sm-4 col-form-label">CTe: </label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control-plaintext" id="cte" name="cte" placeholder="link CTe" value="${tripCheck.url_CTE != null ? tripCheck.url_CTE: '' }">
                                                </div>
                                            </div>`
               
            } else {
                if(tripCheck.url_NFE && tripCheck.url_NFE != null) {
                    formTripCheck.innerHTML += `<div class="form-group row">
                                                    <label class="col-sm-4 col-form-label"> <a href="${tripCheck.url_NFE}" target="_blank"> NFE </a> </label>
                                                </div>`
                }
                formTripCheck.innerHTML += `<div class="form-group row">
                                                <label class="col-sm-4 col-form-label">NFE: </label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control-plaintext" id="nfe" name="nfe" placeholder="link NFE" value="${tripCheck.url_NFE != null ? tripCheck.url_NFE : ''}">
                                                </div>
                                            </div>`
                
            }
            formTripCheck.innerHTML += `<button type="submit" name="updateTripCheck" class="btn btn-primary">Submit</button>`;

            let tableItemsTrip = $('#tableItemsTrip')[0];
            tableItemsTrip.innerHTML = '';
            tableItemsTrip.innerHTML += `<table><thead>
                                            <tr>
                                            <th scope="col">ID Item</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Check</th>
                                            </tr>
                                        </thead>`;

          tableItemsTrip.innerHTML += '<tbody>';

          itemsTrip.forEach(item => {
            let stringRow = `<tr>
            <th scope="row">${item.id}</th>
            <td>${item.product_name}</td>`;

            

            let checked = '';

            if (item.checked == 1) {
                checked = 'checked';
            } 

            stringRow += `<td><input onchange="updateItemCheck(this);" ${checked} data-id-item-trip="${item.id}" type="checkbox"></td>
          </tr>`;

          tableItemsTrip.innerHTML += stringRow;

          });

          tableItemsTrip.innerHTML += '</tbody></table>';

        })
    });

    
});

