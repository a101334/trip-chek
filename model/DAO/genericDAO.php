<?php 

    class GenericDAO {
        protected $conn;

        protected function getConnection() {
            try {
                $serverName = 'localhost';
                $userName = 'developer';
                $password = 'developer';

                $this->conn = new PDO('mysql:host='.$serverName, $userName, $password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (PDOException $e) {
                echo 'error: '.$e->getMessage();
            }
        }

        protected function closeConnection() {
            if(!is_null($this->conn)) {
                $this->conn = null;
            }
        }
    }

?>