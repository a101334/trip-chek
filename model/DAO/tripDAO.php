<?php
    require_once __DIR__ . '/genericDAO.php';

    class TripDAO extends GenericDAO {

        function getAllItemsFromTrip($idTrip) {
            try {
                $this->getConnection();
                $sqlQuery = 'SELECT * FROM corporation.trip_item WHERE id_trip = :id_trip';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':id_trip', $idTrip);
                $stm->execute();
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);
                return $result;
                $this->closeConnection();

            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }

        function getTripCheck($idTrip) {
            try {
                $this->getConnection();
                $sqlQuery = 'SELECT tc.*, t.type_trip, e.name FROM corporation.trip_check tc INNER JOIN corporation.employee e ON  tc.id_employee = e.id INNER JOIN corporation.trip t ON tc.id_trip = t.id AND id_trip = :id_trip ORDER BY tc.id DESC LIMIT 1';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':id_trip', $idTrip);
                $stm->execute();
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);
                $this->closeConnection();
                return $result;

            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }

        function createTripCheckEmpty($idTrip, $idEmployee) {
            try {
                $this->getConnection();
                $sqlQuery = 'INSERT INTO corporation.trip_check (id_trip, id_employee) VALUES (:id_trip, :id_employee)';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':id_trip', $idTrip);
                $stm->bindValue(':id_employee', $idEmployee);
                $stm->execute();
                $this->closeConnection();
                return $this->getTripCheck($idTrip);

            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }

        function updateCheckItemValue($idItem, $checkValue) {
            try {
                $this->getConnection();
                $sqlQuery = 'UPDATE corporation.trip_item SET checked=:checked WHERE id=:idItem';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':idItem', $idItem);
                $stm->bindValue(':checked', $checkValue);
                $stm->execute();
                $this->closeConnection();
            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }

        function isItemsTripCheck($idTrip) {
            try {
                $this->getConnection();
                $sqlQuery = 'SELECT checked FROM corporation.trip_item WHERE checked = 0 AND id_trip = :id_trip';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':id_trip', $idTrip);
                $stm->execute();
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);
                $this->closeConnection();
                return $result;

            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }
        
        function updateTripCheck($idTrip, $status, $urlNFE, $urlCTE, $urlManifesto) {
            try {
                $this->getConnection();
                $sqlQuery = 'UPDATE corporation.trip_check SET status=:status, url_NFE=:urlNFE, url_CTE=:urlCTE, url_manifesto=:urlManifesto WHERE id_trip=:idTrip';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':idTrip', $idTrip);
                $stm->bindValue(':status', $status);
                $stm->bindValue(':urlNFE', $urlNFE);
                $stm->bindValue(':urlCTE', $urlCTE);
                $stm->bindValue(':urlManifesto', $urlManifesto);
                $stm->execute();
                $this->closeConnection();
            } catch (Exception $e) {
                echo $e->getMessage().'<br>';
            }
        }
    }

?>