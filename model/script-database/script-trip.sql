DROP DATABASE IF EXISTS corporation;
CREATE DATABASE corporation;

USE corporation;

DROP TABLE IF EXISTS corporation.employee;
CREATE TABLE corporation.employee (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    function VARCHAR (50),
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS corporation.trip;
CREATE TABLE corporation.trip(
    id INT NOT NULL AUTO_INCREMENT,
    id_employee INT NOT NULL,
    type_trip ENUM('importation', 'exportation'),
    PRIMARY KEY (id),
    FOREIGN KEY (id_employee) REFERENCES corporation.employee(id)
);

DROP TABLE IF EXISTS corporation.trip_item;
CREATE TABLE corporation.trip_item (
    id INT NOT NULL AUTO_INCREMENT,
    id_trip INT NOT NULL,
    product_name VARCHAR(50) NOT NULL,
    checked TINYINT NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    FOREIGN KEY (id_trip) REFERENCES corporation.trip(id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS corporation.trip_check;
CREATE TABLE corporation.trip_check (
    id INT NOT NULL AUTO_INCREMENT,
    id_trip INT NOT NULL,
    id_employee INT NOT NULL,
    status ENUM ('Approved', 'Pending') NOT NULL DEFAULT  'Pending',
    url_NFE VARCHAR(100),
    url_CTE VARCHAR(100),
    url_manifesto VARCHAR(100),
    PRIMARY KEY (id),
    FOREIGN KEY (id_trip) REFERENCES corporation.trip(id),
    FOREIGN KEY (id_employee) REFERENCES corporation.employee(id)
);

INSERT INTO corporation.employee (name, function) 
VALUES ('Alex', 'Motorista'),
('Rafael', 'Motorista'),
('Sr. Costa', 'Fiscal');

INSERT INTO corporation.trip (id_employee, type_trip)
VALUES (1, 'importation'),
(2, 'exportation');

INSERT INTO corporation.trip_item (id_trip, product_name, checked)
VALUES (1, 'product import 1', 0),
(1, 'product import 2', 1),
(1, 'product import 3', 0),
(1, 'product import 4', 0),
(1, 'product import 5', 0),
(1, 'product import 6', 0),
(1, 'product import 7', 0),
(1, 'product import 8', 0),
(2, 'product export 1', 0),
(2, 'product export 2', 1),
(2, 'product export 3', 0),
(2, 'product export 4', 0),
(2, 'product export 5', 0);